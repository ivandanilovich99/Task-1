export buildDate=$(date '+%Y-%m-%d_%H:%M:%S') 
export commitSHA=$(git rev-parse HEAD)


cd ./api
docker buildx build --platform linux/amd64 \
             --build-arg buildDate=${buildDate} \
             --build-arg commitSHA=${commitSHA} \
             . \
             -t task-1_api

cd ../frontend
docker buildx build --platform linux/amd64 \
             --build-arg buildDate=${buildDate} \
             --build-arg commitSHA=${commitSHA} \
             . \
             -t task-1_frontend